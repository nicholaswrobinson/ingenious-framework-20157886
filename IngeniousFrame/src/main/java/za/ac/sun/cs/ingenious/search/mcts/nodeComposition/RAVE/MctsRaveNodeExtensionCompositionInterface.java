package za.ac.sun.cs.ingenious.search.mcts.nodeComposition.RAVE;

import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeComposition;

public interface MctsRaveNodeExtensionCompositionInterface {

    String getID();

    void setUp(MctsNodeComposition node);

}