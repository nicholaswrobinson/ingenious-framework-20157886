package za.ac.sun.cs.ingenious.core.commandline.gameserver;

import com.esotericsoftware.minlog.Log;

import java.io.IOException;
import java.util.Map;

import za.ac.sun.cs.ingenious.core.RefereeFactory;
import za.ac.sun.cs.ingenious.core.network.lobby.GameServer;
import za.ac.sun.cs.ingenious.core.util.Constants;
import za.ac.sun.cs.ingenious.logging.Logger;

/**
 * Class for starting a GameServer. Hostname and port can be specified via
 * commandline parameters. Using the -C commandline parameter, additional
 * referees can be registered.
 * 
 * Created by Chris Coetzee on 2016/07/27.
 */
public class Driver {

    public static void main(String[] args) {
        Log.setLogger(new Logger(Constants.LogDirectory, "core.commandline.gameserver.Driver"));
        Log.info("GameServer commandline script started");
        
        GameServerArguments serverArgs = new GameServerArguments();
        serverArgs.processArguments(args);

        for (Map.Entry<String, String> controller : serverArgs.getControllers().entrySet()) {
	        try {
				RefereeFactory.getInstance().registerNewReferee(controller.getKey(),
				                                                      controller.getValue());
			} catch (ClassNotFoundException | ClassCastException e) {
				Log.error("Could not register new referee, message: " + e.getMessage());
				Log.info("Will continue without registering referee.");
			}            
        }
        GameServer server;
		try {
			server = new GameServer(serverArgs.getHostname(), serverArgs.getPort());
			server.run();
			server.close();
		} catch (IOException e) {
			Log.error("Could not start GameServer. Aborting.");
			System.exit(-1);
		}
    }
}
