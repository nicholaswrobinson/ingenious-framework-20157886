package za.ac.sun.cs.ingenious.games.ingenious;
/**
 * BagSequence interface provides a
 * sequence of tiles that can be drawn and viewed
 * @author steven
 **/
public interface BagSequence {
	public int size();
	public Tile draw();
	public Tile[] viewVisible(int lookAhead);
	public boolean isEmpty();
}
