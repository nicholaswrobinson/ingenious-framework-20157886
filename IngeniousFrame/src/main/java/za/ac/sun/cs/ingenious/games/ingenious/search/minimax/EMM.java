package za.ac.sun.cs.ingenious.games.ingenious.search.minimax;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;

import za.ac.sun.cs.ingenious.games.ingenious.IngeniousAction;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousEngine;
import za.ac.sun.cs.ingenious.games.ingenious.Tile;
import za.ac.sun.cs.ingenious.games.ingenious.engines.IngeniousEvaluator;


public class EMM extends MiniMax {

	public EMM(int depth){
		this.searchDepth = depth;
		
	}
	
	private final static ArrayList<Tile> possibleTiles = new ArrayList<Tile>();
	
	private int calcPossibleTiles(int numberOfColours){
		return (numberOfColours*(numberOfColours+1))/2;
	}
	private static Tile getPossibleTile(int numberInPossibleTiles){
		
		return possibleTiles.get(numberInPossibleTiles);
		
	}
	
	@Override
	public IngeniousAction generateMove(IngeniousEngine engine, IngeniousEvaluator evaluator) {

		this.evaluator = evaluator;

		/*
		 * The gameState contains all information that changes at different
		 * ply's.
		 */
		SearchState gameState = getState(engine);

		/*
		 * set the localVariables which are used throughout the logginsearch without
		 * being changed.
		 */
		this.evaluator = evaluator;
		setLocalVariables(engine);



		ArrayList<IngeniousAction> moves = gameState.generateMoves();

		IngeniousAction bestMove = moves.get(0);
		double bestValue = Double.MIN_VALUE+1;
		double alpha = Double.MIN_VALUE+1;
		double beta = Double.MAX_VALUE;
		
		for (IngeniousAction child : moveOrder(moves,gameState)) {
			
			this.preSearchStateAdjustment(gameState);
			makeMove(child, gameState);
			double value = chanceNode(gameState,0,alpha,beta, true);
			unmakeMove(child, gameState);

			this.postSearchStateAdjustment(gameState);

			if (bestValue < value) {
				bestValue = value;
				bestMove = child;
			}
			
			alpha = Math.max(alpha, bestValue);
			if(bestValue >=beta){
				break;
			}

		}

		return bestMove;
	}
	
	@Override
	public void setLocalVariables(IngeniousEngine engine) {

		this.playerId = engine.getPlayerID();
		this.opponentId = 1 - this.playerId;
		this.numberOfColours = engine.matchSetting.getNumColours();
		for(int i = 0; i< numberOfColours; i++){
			for(int j = 0; j<numberOfColours;j++){
				if(i>=j){
					possibleTiles.add(new Tile(i,j));
				}
			}
		}
	}
	
	/**
	 * Make move includes all affects to the state of the game not just the
	 * placement of a tile on the board
	 */
	@Override
	public void makeMove(IngeniousAction child, SearchState gameState) {

		try {

			// TODO commented out to avoid compile errors, sorry...
//			gameState.getBoard().makeMove(child);
			gameState.getRacks().get(gameState.getCurrentPlayer()).remove(child.getTile());

			
			gameState.getScores().updateScore(playerId, gameState.getBoard());
			gameState.setCurrentPlayer(1 - gameState.getCurrentPlayer());
		} catch (ClassCastException e) {
			throw new RuntimeException(
					"SearchState not compatible with  search.minimax enhancements");
		}

	}

	@Override
	public void unmakeMove(IngeniousAction child, SearchState gameState) {
		try {
	
			gameState.setCurrentPlayer(1 - gameState.getCurrentPlayer());

			// TODO commented out to avoid compile errors, sorry...
//			gameState.getBoard().undoMove();
			gameState.getRacks().get(gameState.getCurrentPlayer()).add(child.getTile());

			gameState.getScores().undoScore(gameState.getCurrentPlayer(), gameState.getBoard());
		} catch (ClassCastException e) {
			throw new RuntimeException(
					"SearchState not compatible with  search.minimax enhancements");
		}
	}

	
	private double chanceNode(SearchState state, int depth, double alpha,double beta, boolean isMax){
		if(state.getBoard().full() || depth == this.searchDepth){
			return evaluator.evaluate(state.getScores(), state.getBoard(), state.getRacks(), state.getCurrentPlayer());
		}
		
		int numberOfPossibleTiles = this.calcPossibleTiles(state.getBoard().getNumColours());
		double sum = 0;
		//Log.info("Number of possible tiles :"+numberOfPossibleTiles);
		for (int i =0; i< numberOfPossibleTiles;i++){
			state.getRacks().get(state.getCurrentPlayer()).add(getPossibleTile(i));
			if(isMax){
				sum = sum + MinMove(state, alpha, beta,depth+1);
			}else{
				sum = sum + MaxMove(state, alpha, beta,depth+1);
			}
			
			state.getRacks().get(state.getCurrentPlayer()).remove(getPossibleTile(i));
			
		}
		double weightedAverage = sum/numberOfPossibleTiles;
		
		return weightedAverage;
	}
	
	@Override
	public double MaxMove(SearchState state,double alpha,double beta, int depth) {

		if (state.getBoard().full() || this.searchDepth == depth) {
			return evaluator.evaluate(state.getScores(), state.getBoard(), state.getRacks(),this.playerId);
		}

		double bestValue =  Double.MIN_VALUE+1;

	//	Log.info("Racksize at max node "+state.getRacks().getInstance(state.getCurrentPlayer()).size());
		Log.info("Current Player"+state.getCurrentPlayer());
		for(Tile t : state.getRacks().get(state.getCurrentPlayer()) ){
			Log.info(t);
		}
		ArrayList<IngeniousAction> moves = state.generateMoves();
		for (IngeniousAction child : moveOrder(moves,state)) {
			this.preSearchStateAdjustment(state);

			makeMove(child, state);
			double value = chanceNode(state,depth +1,alpha,beta, true );
			unmakeMove(child, state);

			this.postSearchStateAdjustment(state);

			if (bestValue < value) {
				bestValue = value;
			}
			
			alpha = Math.max(alpha, bestValue);
			if(bestValue >=beta){
				break;
			}
		}

		return bestValue;
	}

	@Override
	public double MinMove(SearchState state,double alpha,double beta, int depth) {

		if (state.getBoard().full() || this.searchDepth == depth) {
			return evaluator.evaluate(state.getScores(), state.getBoard(), state.getRacks(),this.playerId);
		}

		double bestValue =  Double.MAX_VALUE;

	//	Log.info("Racksize at min node "+state.getRacks().getInstance(state.getCurrentPlayer()).size());
		ArrayList<IngeniousAction> moves = state.generateMoves();

		for (IngeniousAction child : moveOrder(moves,state)) {
			this.preSearchStateAdjustment(state);

			makeMove(child, state);
			double value = chanceNode(state,depth +1,alpha,beta, false);
			unmakeMove(child, state);

			this.postSearchStateAdjustment(state);

			if (bestValue > value) {
				bestValue = value;
			}
			
			beta = Math.min(beta, bestValue);
			if(bestValue <=alpha){
				break;
			}

		}

		return bestValue;
	}
	
	public static void main(String [] args){

		
	}
}
