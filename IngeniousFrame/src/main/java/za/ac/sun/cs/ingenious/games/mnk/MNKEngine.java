package za.ac.sun.cs.ingenious.games.mnk;

import com.esotericsoftware.minlog.Log;

import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.util.message.MatchSettingsInitGameMessage;

public abstract class MNKEngine extends Engine {
	
	protected MNKState board;
	protected MNKLogic logic;
	protected MNKFinalEvaluator eval;
	
	public MNKEngine(EngineToServerConnection toServer){
		super(toServer);
		board = null;
		logic = new MNKLogic();
		eval = new MNKFinalEvaluator();
	}
	
	@Override
	public void receiveInitGameMessage(InitGameMessage a) {
		MatchSetting ms = ((MatchSettingsInitGameMessage) a).getSettings();
		int h,w,k;
		boolean p;
		try {
			h = ms.getSettingAsInt("mnk_height");
			w = ms.getSettingAsInt("mnk_width");
			k = ms.getSettingAsInt("mnk_k");
			p = ms.getSettingAsBoolean("perfectInformation");
			this.board = new MNKState(h,w,k,p, ms.getNumPlayers());
		} catch (MissingSettingException | IncorrectSettingTypeException e) {
			Log.error("Could not read settings from received MatchSetting object, message: " + e.getMessage());
		}
	}
	
	@Override
	public void receivePlayedMoveMessage(PlayedMoveMessage a) {
		logic.makeMove(board, a.getMove());
	}

	@Override
	public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
		Log.info("Game terminated.");
		Log.info("Final scores for my terminal position:");
		double[] score = eval.getScore(board);
		for (int i = 0; i < score.length; i++) {
			Log.info("Player " + i + ": " + score[i]);
		}
		Log.info("Final state as seen by player " + this.playerID + ":");
		board.printPretty();
		toServer.closeConnection();
	}
}
