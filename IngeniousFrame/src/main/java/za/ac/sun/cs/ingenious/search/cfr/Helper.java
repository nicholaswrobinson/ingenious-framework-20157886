package za.ac.sun.cs.ingenious.search.cfr;

import com.esotericsoftware.minlog.Log;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import za.ac.sun.cs.ingenious.core.Action;

/**
 * Provides common methods for all CFR based search algorithms.
 */
public final class Helper {

	// Tolerance with which to check double values for equality.
	public static final double floatingTolerance = 0.0000001;

	private Helper() {}

	/**
	 * @param dist Probability distribution over actions for some information set.
	 * @return An action sampled according to the given probability distribution
	 */
	public static Action sampleFromStrategy(Map<Action, Double> dist) {
		Action sampledAction = null;
		double sampleValue = Math.random();
		double cumulativeProbs = 0.0;
		Set<Action> actions = dist.keySet();
		for (Action a: actions) {
			cumulativeProbs += dist.get(a);
			if (sampledAction == null && cumulativeProbs > sampleValue) {
				sampledAction = a;
			}
		}
		if (!(1.0 - floatingTolerance < cumulativeProbs && cumulativeProbs < 1.0 + floatingTolerance)) {
			Log.error("Helper.sampleFromStrategy", "Cumulative probability is: " + cumulativeProbs);				
		}
		return sampledAction;
	}
	
	
	/**
	 * @param unnormalizedTable An unnormalized probability "distribution" over actions for some information set.
	 * @return The input table normalized with the sum of all "probabilities" over all actions.
	 */
	public static Map<Action, Double> getAverageStrategyProfile(Map<Action, Double> unnormalizedTable) {
		double sum = 0.0;
		for (Double d : unnormalizedTable.values()) {
			sum += d;
		}
		
		Map<Action, Double> normalizedTable = new HashMap<Action, Double>();
		for (Entry<Action,Double> e : unnormalizedTable.entrySet()) {
			normalizedTable.put(e.getKey(), e.getValue()/sum);
		}
		
		return normalizedTable;
	}
	
}
