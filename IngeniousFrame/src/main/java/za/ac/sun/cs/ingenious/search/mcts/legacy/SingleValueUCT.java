package za.ac.sun.cs.ingenious.search.mcts.legacy;

import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;

/**
 * This Descender is suitable for two player zero sum games with alternating moves. Instead of
 * two score values for the players, only one score is stored per node. So we only have to look
 * at that one score. This should be used together with {@link NegamaxUpdater}.
 * 
 * @author Michael Krause
 */
public class SingleValueUCT<S extends TurnBasedGameState, N extends SearchNode<S,N>> extends UCT<S,N> {

	public SingleValueUCT(double c) {
		super(c);
	}

	@Override
	public double get(N node, int forPlayerID, int totalNofPlayouts) {
		return super.get(node, 0, totalNofPlayouts);
	}
	
}
