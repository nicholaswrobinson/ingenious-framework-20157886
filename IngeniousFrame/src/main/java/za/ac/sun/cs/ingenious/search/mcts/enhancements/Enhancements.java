package za.ac.sun.cs.ingenious.search.mcts.enhancements;

import java.util.List;
import java.util.ArrayList;

import java.lang.reflect.Method;
import java.util.function.Consumer;
import java.util.function.BiConsumer;

import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeCompositionInterface;
import za.ac.sun.cs.ingenious.core.GameState;

public class Enhancements<S extends GameState, C, N extends MctsNodeCompositionInterface<S, C, ?>> {
	
	private static boolean PRE_SELECTION_ENHANCEMENT;
	private static boolean POST_SELECTION_ENHANCEMENT;
	private static boolean PRE_EXPANSION_ENHANCEMENT;
	private static boolean POST_EXPANSION_ENHANCEMENT;
	private static boolean PRE_SIMULATION_ENHANCEMENT;
	private static boolean POST_SIMULATION_ENHANCEMENT;
	private static boolean PRE_BACKPROP_ENHANCEMENT;
	private static boolean POST_BACKPROP_ENHANCEMENT;
	
	private ArrayList<Consumer<N>> preSelectionList;
	private ArrayList<BiConsumer<N, C>> postSelectionList;
	private ArrayList<Consumer<N>> preExpansionList;
	private ArrayList<BiConsumer<N, C>> postExpansionList;
	private ArrayList<Consumer<S>> preSimulationList;
	private ArrayList<Consumer<double[]>> postSimulationList;
	private ArrayList<BiConsumer<N, double[]>> preBackpropagationList;
	private ArrayList<Consumer<N>> postBackpropagationList;
	

	public Enhancements(List<EnhancementThreadSafe<S , C, N>> enhancements) {
		preSelectionList = new ArrayList<Consumer<N>>();
		postSelectionList = new ArrayList<BiConsumer<N, C>>();
		preExpansionList = new ArrayList<Consumer<N>>();
		postExpansionList = new ArrayList<BiConsumer<N, C>>();
		preSimulationList = new ArrayList<Consumer<S>>();
		postSimulationList = new ArrayList<Consumer<double[]>>();
		preBackpropagationList = new ArrayList<BiConsumer<N, double[]>>();
		postBackpropagationList = new ArrayList<Consumer<N>>();
		
		if (enhancements == null) {
			return;
		}
		
		for (EnhancementThreadSafe<S, C, N> e : enhancements) {
		
			Method[] allMethods = e.getClass().getDeclaredMethods();
			for (Method m : allMethods) {
				
				switch(m.getName()) {
					case "preSelection" : 
						preSelectionList.add(e::preSelection);
						break;
					case "postSelection" : 
						postSelectionList.add(e::postSelection);
						break;
					case "preExpansion" :
						preExpansionList.add(e::preExpansion);
						break;
					case "postExpansion" :
						postExpansionList.add(e::postExpansion);
						break;
					case "preSimulation" :
						preSimulationList.add(e::preSimulation);
						break;	
					case "postSimulation" :
						postSimulationList.add(e::postSimulation);
						break;	
					case "preBackpropagation" :
						preBackpropagationList.add(e::preBackpropagation);
						break;
					case "postBackpropagation" :
						postBackpropagationList.add(e::postBackpropagation);
						break;
				} // end of switch
			} // end of for (methods)
		} // end of for (enhancements)
		
		booleanSetup();
	} 
	
	private void booleanSetup() {
		PRE_SELECTION_ENHANCEMENT = (preSelectionList.isEmpty()) ? false : true;
		POST_SELECTION_ENHANCEMENT = (postSelectionList.isEmpty()) ? false : true;
		PRE_EXPANSION_ENHANCEMENT = (preExpansionList.isEmpty()) ? false : true;
		POST_EXPANSION_ENHANCEMENT = (postExpansionList.isEmpty()) ? false : true;
		PRE_SIMULATION_ENHANCEMENT = (preSimulationList.isEmpty()) ? false : true;
		POST_SIMULATION_ENHANCEMENT = (postSimulationList.isEmpty()) ? false : true;
		PRE_BACKPROP_ENHANCEMENT = (preBackpropagationList.isEmpty()) ? false : true;
		POST_BACKPROP_ENHANCEMENT = (postBackpropagationList.isEmpty()) ? false : true;
	}
	
	/***** List getters: ******************************************************/
	
	public List<Consumer<N>> getPreSelectionList() {
		return preSelectionList;
	}
	
	public List<BiConsumer<N, C>> getPostSelectionList() {
		return postSelectionList;
	}
	
	public List<Consumer<N>> getPreExpansionList() {
		return preExpansionList;
	}
	
	public List<BiConsumer<N, C>> getPostExpansionList() {
		return postExpansionList;
	}
	
	public List<Consumer<S>> getPreSimulationList() {
		return preSimulationList;
	}
	
	public List<Consumer<double[]>> getPostSimulationList() {
		return postSimulationList;
	}
	
	public List<BiConsumer<N, double[]>> getPreBackpropagationList() {
		return preBackpropagationList;
	}
	
	public List<Consumer<N>> getPostBackpropagationList() {
		return postBackpropagationList;
	}
	
	/***** Boolean getters: ***************************************************/
	
	public boolean getPreSelectionBool() {
		return PRE_SELECTION_ENHANCEMENT;
	}
	
	public boolean getPostSelectionBool() {
		return POST_SELECTION_ENHANCEMENT;
	}
	
	public boolean getPreExpansionBool() {
		return PRE_EXPANSION_ENHANCEMENT;
	}
	
	public boolean getPostExpansionBool() {
		return POST_EXPANSION_ENHANCEMENT;
	}
	
	public boolean getPreSimulationBool() {
		return PRE_SIMULATION_ENHANCEMENT;
	}
	
	public boolean getPostSimulationBool() {
		return POST_SIMULATION_ENHANCEMENT;
	}
	
	public boolean getPreBackpropagationBool() {
		return PRE_BACKPROP_ENHANCEMENT;
	}
	
	public boolean getPostBackpropagationBool() {
		return POST_BACKPROP_ENHANCEMENT;
	}
	
	/***** Apply methods: *****************************************************/
	
	public void applyPreSelection(N node) {
		for (Consumer<N> f : preSelectionList) {
			f.accept(node);
		}
	}
	
	public void applyPostSelection(N node, C child) {
		for (BiConsumer<N, C> f : postSelectionList) {
			f.accept(node, child);
		}
	}
	
	public void applyPreExpansion(N node) {
		for (Consumer<N> f : preExpansionList) {
			f.accept(node);
		}
	}
	
	public void applyPostExpansion(N node, C child) {
		for (BiConsumer<N, C> f : postExpansionList) {
			f.accept(node, child);
		}
	}
	
	public void applyPreSimulation(S state) {
		for (Consumer<S> f : preSimulationList) {
			f.accept(state);
		}
	}
	
	public void applyPostSimulation(double[] results) {
		for (Consumer<double []> f : postSimulationList) {
			f.accept(results);
		}
	}
	
	
	public void applyPreBackpropagation(N node, double[] results) {
		for (BiConsumer<N, double[]> f : preBackpropagationList) {
			f.accept(node, results);
		}
	}
	
	public void applyPostBackpropagation(N node) {
		for (Consumer<N> f : postBackpropagationList) {
			f.accept(node);
		}
	}
	
}

//TODO: comment
//TODO: clean upc  
