package za.ac.sun.cs.ingenious.games.othello.engines;

import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.esotericsoftware.minlog.Log;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.move.ForfeitAction;
import za.ac.sun.cs.ingenious.core.util.move.IdleAction;
import za.ac.sun.cs.ingenious.core.util.move.XYAction;

import za.ac.sun.cs.ingenious.games.othello.engines.OthelloEngine;
import za.ac.sun.cs.ingenious.games.othello.network.*;

/**
 * The human engine class for the Othello (Reversi) board game. 
 * The engine represents a player and receives game updates and requests
 * from the Referee (server).
 */
public class OthelloHumanEngine extends OthelloEngine {

	/**
	 * Constructs a new engine.
	 *
	 * @param serverConnection	the connection the engine uses to receive and send
	 *							information from and to the server
	 */
	public OthelloHumanEngine(EngineToServerConnection serverConnection)
	{
		super(serverConnection);
	}

	@Override
	public void setZobrist(ZobristHashing zobristHashing) {

	}


	/**
	 * Returns the Engine's name
	 *
	 * @return	the engine's name
	 */
	public String engineName()
	{
		return "Othello Human Engine";
	}

	/**
	 * Requests a move from the player and determines if the move is valid.
	 * If the move is valid, it is sent to the Referee. Otherwise the player
	 * is asked to generate a new move until the player's clock runs out or
	 * the maximum wrong moves is reached.
	 *
	 * @param message	the generate move request sent from the Referee
	 *
	 * @return			a valid move, or null if the clock ran out, the
	 *					player's tries ran out, or the player has forfeit
	 */
	public PlayActionMessage receiveGenActionMessage(GenActionMessage message) {
		/* TODO: determine if the player can move, if not generate an IdleMove */
		if (output) {
			board.printPretty();
		}

		/* TODO: handle clock and timeout */
		Action action = null;
		int triesLeft = 10;

		String input = "";
		String split[];
		int moveRow, moveCol;

		while (triesLeft > 0) {
			try {
				input = promptInput("Enter a row and column to place a disk");
				split = input.split(" ");

				if (split.length <= 0 || split[0].length() <= 0) {
					Log.error("No move input. Try again.");
					triesLeft--;
					continue;
				}

				if (split[0].charAt(0) == 'x') {
					Log.info("Quiting game.");
					action = new ForfeitAction((byte) playerID);
					break;
				}

				moveRow = Integer.parseInt(split[0]);
				moveCol = Integer.parseInt(split[1]);

				if (moveRow >= 0 && moveCol >= 0) {
					action = new XYAction(moveCol, moveRow, playerID);
					
					if (action == null || !logic.validMove(board, action)) {
						Log.error("Invalid move input. Try again.");
						triesLeft--;
						continue;
					}

					break;
				} else if (moveRow == -1 || moveCol == -1) {
					action = new IdleAction((byte) playerID);
					
					if (action == null || !logic.validMove(board, action)) {
						Log.error("Invalid move input. Try again.");
						triesLeft--;
						continue;
					}

					break;
				} else {
					Log.error("Invalid move input. Try again.");
					triesLeft--;
					continue;
				}
			} catch (Exception e) {
				Log.error("Invalid input with exception:", e);
				Log.error("Try again.");
				triesLeft--;
				continue;
			}
		}

		if (triesLeft <= 0) {
			Log.error("You have no more tries left.");
			action = new ForfeitAction((byte) playerID);
		}

		return new PlayActionMessage(action);
	}

	/**
	 * Sends a request to the input medium for the user's input.
	 *
	 * @param	promptMessage	the message to display to the user
	 *
	 * @return	the input string of the user
	 */
	private String promptInput(String promptMessage) throws IOException, InterruptedException
	{
		String input = "";

		/* TODO: TUI things */
		
		Log.info(promptMessage);

		/* very verbose, but safe, read from system.in */
		synchronized (System.in) {
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

			while (!in.ready()) {
				synchronized (this) {
					wait(10);
				}
			}

			if (in.ready()) {
				input = in.readLine();
			}
		} 

		return input;
	}
}
