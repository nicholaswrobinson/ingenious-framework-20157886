package za.ac.sun.cs.ingenious.search.mcts.simulation;

import za.ac.sun.cs.ingenious.core.Action;
import java.util.ArrayList;

public class SimulationTuple {

    private ArrayList<Action> moves;
    private double[] scores;

    public SimulationTuple(ArrayList<Action> moves, double[] scores) {
        this.moves = moves;
        this.scores = scores;
    }

    public double[] getScores() {
        return scores;
    }

    public ArrayList<Action> getMoves() {
        return moves;
    }

}
