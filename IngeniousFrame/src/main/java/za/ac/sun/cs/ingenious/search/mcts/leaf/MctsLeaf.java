package za.ac.sun.cs.ingenious.search.mcts.leaf;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.search.mcts.enhancements.Enhancements;
import za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.selection.SelectionThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeCompositionInterface;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Action;
import java.util.List;
import java.util.ArrayList;
import java.lang.Thread;
import java.lang.InterruptedException;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.Callable;

// needed for default constructor:
import za.ac.sun.cs.ingenious.core.util.search.mcts.MctsGameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import static za.ac.sun.cs.ingenious.search.mcts.selection.SelectionUct.newSelectionUct;
import static za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionSingle.newExpansionSingle;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationRandom.newSimulationRandom;
import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationAverage.newBackpropagationAverage;

// needed for enhancements:
import za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationTuple;

/**
 * Implements a general-purpose leaf parallelism version of the MCTS algorithm.
 * This implementation uses Threads and is for multicore systems with shared 
 * memory. Due to the shared memory nature of this algorithm, the MCTS node type 
 * that can be used for this algorithm should store the child and parent of the 
 * node as references to the node structure.
 * 
 * @author Karen Laubscher
 * 
 * @param <S>  The game state type
 * @param <N>  The MCTS node type
 */
public class MctsLeaf<S extends GameState, N extends MctsNodeCompositionInterface<S, N, N>> {
    
    private static boolean PRE_SELECTION_ENHANCEMENT;
	private static boolean POST_SELECTION_ENHANCEMENT;
	private static boolean PRE_EXPANSION_ENHANCEMENT;
	private static boolean POST_EXPANSION_ENHANCEMENT;
	private static boolean PRE_SIMULATION_ENHANCEMENT;
	private static boolean POST_SIMULATION_ENHANCEMENT;
	private static boolean PRE_BACKPROP_ENHANCEMENT;
	private static boolean POST_BACKPROP_ENHANCEMENT;
	
	Enhancements<S, N, N> enhancements = null;
    
    private SelectionThreadSafe<N, N> selection;
	private ExpansionThreadSafe<N, N> expansion;
	private SimulationThreadSafe<S> simulation;
	private BackpropagationThreadSafe<N> backpropagation;
	private SelectionThreadSafe<N, N> finalSelection;
	private GameLogic<S> logic;
    private int threadCount;
    private ExecutorService executor;
    
    /** 
	 * Default constructor, which uses SelectionUct, ExpansionSingle,
	 * SimulationRandom and BackpropagationAverage. The final selection strategy 
	 * is also the same as the selection strategy.
	 *
	 * @param logic	The game logic, used to check for teminal gamestates.
	 * @param threadCount
	 *				The number of simulations that run during the simulation
	 *				step in different threads from the epansion added node 
	 * @param mctsEvaluator	
	 * 				Evaluator that includes values used for mcts simulation results.
	 * @param obs	Object to process actions made by the players during playouts. 
	 *			    Actions are observed from the point of view of the environment player.
	 * @param playerId
	 *				The ID of the player, used during backpropagation.
	 * @param threadCount
	 *					 The number of simulations that run during the simulation
	 *					 step in different threads from the epansion added node
	 */
	public MctsLeaf(GameLogic<S> logic, MctsGameFinalEvaluator<S> mctsEvaluator, ActionSensor<S> obs, int playerId, int threadCount) {
		
		this.selection = newSelectionUct(logic);
		this.expansion = newExpansionSingle(logic);
		this.simulation = newSimulationRandom(logic, mctsEvaluator, obs, false);
		this.backpropagation = newBackpropagationAverage();
		this.finalSelection = this.selection;
		this.logic = logic;
		this.threadCount = threadCount;
		this.executor = Executors.newFixedThreadPool(threadCount);
		enhancementSetup(null);
	}
	
	/** 
	 * Default constructor, also allowing the use of enhancements and uses 
	 * SelectionUct, ExpansionSingle, SimulationRandom and BackpropagationAverage.
	 * The final selection strategy is also the same as the selection strategy. 
	 *
	 * @param logic	The game logic, used to check for teminal gamestates.
	 * @param mctsEvaluator	
	 * 				Evaluator that includes values used for mcts simulation results.
	 * @param obs	Object to process actions made by the players during playouts. 
	 *			    Actions are observed from the point of view of the environment player.
	 * @param playerId
	 *				The ID of the player, used during backpropagation.
	 * @param threadCount
	 *					 The number of simulations that run during the simulation
	 *					 step in different threads from the epansion added node
	 * @param enhancements
	 *				An enhancements object containing the chosen algorithm 
	 *				enhancements combination desired to be used with the mcts.
	 */
	 public MctsLeaf(GameLogic<S> logic, MctsGameFinalEvaluator<S> mctsEvaluator, ActionSensor<S> obs, int playerId, int threadCount, Enhancements<S, N, N> enhancements) {
		
		this.selection = newSelectionUct(logic);
		this.expansion = newExpansionSingle(logic);
		this.simulation = newSimulationRandom(logic, mctsEvaluator, obs, false);
		this.backpropagation = newBackpropagationAverage();
		this.finalSelection = this.selection;
		this.logic = logic;
		this.threadCount = threadCount;
		this.executor = Executors.newFixedThreadPool(threadCount);
		this.enhancements = enhancements;
		enhancementSetup(enhancements);
	}
    
    /**
	 * Constructor that takes in the 4 basic strategies (where final selection 
	 * is the same as the general selection strategy).
	 *
	 * @param selection  The selection strategy. A common strategy to use is 
	 *					 {@link SelectionUct}, which calculates UCT values.
	 * @param expansion	 The expansion strategy. A common strategy to use is 
	 *  				 {@link ExpansionSingle}, which adds one unexplored node
	 * @param simulation The simulation strategy. A basic play out policy is to 
	 *					 use {@link SimulationRandom}, which do random playout.
	 * @param backpropagation
	 *                   The backpropagation strategy. A common strategy to use 
	 *					 is {@link BackpropagationAverage}, just adding the result
	 * @param logic 	 The game logic, used to check for ternimal gamestates.
	 * @param threadCount
	 *					 The number of simulations that run during the simulation
	 *					 step in different threads from the epansion added node
	 * @param enhancements
	 *					An enhancements object containing the chosen algorithm 
	 *					enhancements combination desired to be used with the mcts.
	 */
    public MctsLeaf(SelectionThreadSafe<N, N> selection, ExpansionThreadSafe<N, N> expansion, SimulationThreadSafe<S> simulation, BackpropagationThreadSafe<N> backpropagation, GameLogic<S> logic, int threadCount, Enhancements<S, N, N> enhancements) {
		this.selection = selection;
		this.expansion = expansion;
		this.simulation = simulation;
		this.backpropagation = backpropagation;
		this.finalSelection = selection;
		this.logic = logic;
        this.threadCount = threadCount;
		this.executor = Executors.newFixedThreadPool(threadCount);
		this.enhancements = enhancements;
		enhancementSetup(enhancements);
    }
    
    /**
	 * Constructor that takes in the 4 basic strategies as well as a final 
	 * selection strategy, that may differ from the selection strategy used when
	 * traversing the TreeEngine during the MCTS algorithm.
	 *
	 * @param selection  The selection strategy. A common strategy to use is 
	 *					 {@link SelectionUct}, which calculates UCT values.
	 * @param expansion	 The expansion strategy. A common strategy to use is 
	 *  				 {@link ExpansionSingle}, which adds one unexplored node
	 * @param simulation The simulation strategy. A basic play out policy is to 
	 *					 use {@link SimulationRandom}, which do random playout.
	 * @param backpropagation
	 *                   The backpropagation strategy. A common strategy to use 
	 *					 is {@link BackpropagationAverage}, just adding the result
	 * @param finalSelection
	 *				     The selection strategy used to make the final selection
	 *					 to determine the best move to be returned.
	 * @param logic 	 The game logic, used to check for ternimal gamestates.
	 * @param threadCount
	 *					 The number of simulations that run during the simulation
	 *					 step in different threads from the epansion added node
	 * @param enhancements
	 *					An enhancements object containing the chosen algorithm 
	 *					enhancements combination desired to be used with the mcts.
	 */
    public MctsLeaf(SelectionThreadSafe<N, N> selection, ExpansionThreadSafe<N, N> expansion, SimulationThreadSafe<S> simulation, BackpropagationThreadSafe<N> backpropagation, SelectionThreadSafe<N, N> finalSelection, GameLogic<S> logic, int threadCount, Enhancements<S, N, N> enhancements) {
		
		this.selection = selection;
		this.expansion = expansion;
		this.simulation = simulation;
		this.backpropagation = backpropagation;
		this.finalSelection = finalSelection;
		this.logic = logic;
        this.threadCount = threadCount;
        this.executor = Executors.newFixedThreadPool(threadCount);
        this.enhancements = enhancements;
		enhancementSetup(enhancements);
    }
    
    /**
	 * This method generates an action that represents the best move choice 
	 * found. The method is the template operation that drives the leaf 
	 * parallelisation MCTS algorithm.
	 * 
	 * @param root        The root node from which the search should start.
	 * @param turnLength  Time in ms within which the move needs to be decided.
	 *
	 * @return The best move action that was found by the algorithm during the
	 *  	   time of the turn length.
	 */
	public Action doSearch(N root, long turnlength) { // TODO: turnlength, change to clock?
		
		long timeInit = System.currentTimeMillis();
		long endTime = timeInit + turnlength; // TODO: optimise (- final move selecin time)
		
		while (System.currentTimeMillis() < endTime) {
			//root.incVisitCount(); // TODO: move to backprop and remove
			if (PRE_SELECTION_ENHANCEMENT) enhancements.applyPreSelection(root);
			N currentNode = selection.select(root);
			if (POST_SELECTION_ENHANCEMENT) enhancements.applyPostSelection(root, currentNode);

			// Traverse TreeEngine (selection phase):
	        while (currentNode.isUnexploredEmpty() && !logic.isTerminal(currentNode.getState())) {
				N previousNode = currentNode;
				if (PRE_SELECTION_ENHANCEMENT) enhancements.applyPreSelection(previousNode);
				currentNode = selection.select(previousNode);
				if (POST_SELECTION_ENHANCEMENT) enhancements.applyPostSelection(previousNode, currentNode);
	        }
	        
	        // Add a node (expansion phase):
			if (PRE_EXPANSION_ENHANCEMENT) enhancements.applyPreExpansion(currentNode);
			N addedNode = expansion.expand(currentNode); 
			if (POST_EXPANSION_ENHANCEMENT) enhancements.applyPostExpansion(currentNode, addedNode);
			//if (addedNode != null) addedNode.incVisitCount(); // TODO: move to backprop and remove
	        
	        // LEAF PARALLELISATION:
	        // Play a simulation (simulation phase):
	        List<Callable<double[]>> leafCallables = new ArrayList<>(threadCount);
	        for (int i = 0; i < threadCount; i++) {
	        	Callable<double[]> c = new LeafCallable(simulation, addedNode);
				leafCallables.add(c);
			}
			
			// waitUntilDone(); and accumulate results
			double[] results = null;
			try {
				List<Future<double[]>> futures = executor.invokeAll(leafCallables);
				for (Future<double[]> f : futures) {
					results = addResults(results, f.get());
				}
			} catch (InterruptedException ie){
				Log.error("MctsLeaf", "Error, a future was interrupted");
			} catch (Exception e) {
				Log.error("MctsLeaf", "Error, something went wrong with the futures");
			}
						
	        
	        // Propagate results up TreeEngine (BackpropagationThreadSafe phase):
			currentNode = addedNode;
			while (currentNode != root) {
				if (PRE_BACKPROP_ENHANCEMENT) enhancements.applyPreBackpropagation(currentNode, results);
				backpropagation.propagate(currentNode, results);
				if (POST_BACKPROP_ENHANCEMENT) enhancements.applyPostBackpropagation(currentNode);
				currentNode = currentNode.getParent();
			}
		}
	    
	    N finalChoice = finalSelection.select(root);
		Action bestMove = finalChoice.getPrevAction();
		
		return bestMove;
	}
	
	/**
	 * A helper method that adds two result arrays together (used for 
	 * accumulating the results after the simulations have finished running)
	 * 
	 * @param results   The main result array, to which the simulated results
	 * 					values should be added
	 * @param simulationResults
	 *				    The simulated results array that should be added to the
	 *					results.
	 *
	 * @return The result array with the added simulation results
	 */
	private static double[] addResults(double[] results, double[] simulationResults) {
		if (results == null) {
			return simulationResults;
		}
		
		//check same size result arrays
		for (int i = 0; i < results.length; i++) {
			results[i] = results[i] + simulationResults[i];
		}
		return results;
	}
	
	
	/**
	 * The callable leaf task that runs a simulation. This task is a Callable
	 * task that return the result of the simulation as a double[]. 
	 */	
	private class LeafCallable implements Callable<double[]> {
		double[] myResults;
		SimulationThreadSafe<S> simulation;
		N node;
		String myId;
		
		/**
		 * Constructor for the callable leaf task.
		 * 
		 * @param simulation  The simulation strategy used for the simulation
		 * @param node		  The node from which the simulation should be run
		 */
		LeafCallable(SimulationThreadSafe<S> simulation, N node) {
			this.simulation = simulation;
	    	this.node = node;
		}
		
		public double[] call() throws Exception {
			myId = Thread.currentThread().getName();
			if (PRE_SIMULATION_ENHANCEMENT) enhancements.applyPreSimulation(node.getState());
			SimulationTuple resutlsTuple = simulation.simulate(node.getState());
			myResults = resutlsTuple.getScores();
			if (POST_SIMULATION_ENHANCEMENT) enhancements.applyPostSimulation(myResults);
			return myResults;
		}
		
		public double[] getMyResults() {
			return myResults;
		}
		
		public String getMyId() {
			return myId;
		}
		
	}
	
	private void enhancementSetup(Enhancements<S, N, N> enhancements) {
		if (enhancements == null) {
			PRE_SELECTION_ENHANCEMENT = false;
			POST_SELECTION_ENHANCEMENT = false;
			PRE_EXPANSION_ENHANCEMENT = false;
			POST_EXPANSION_ENHANCEMENT = false;
			PRE_SIMULATION_ENHANCEMENT = false;
			POST_SIMULATION_ENHANCEMENT = false;
			PRE_BACKPROP_ENHANCEMENT = false;
			POST_BACKPROP_ENHANCEMENT = false;

		} else {
			PRE_SELECTION_ENHANCEMENT = enhancements.getPreSelectionBool();
			POST_SELECTION_ENHANCEMENT = enhancements.getPostSelectionBool();
			PRE_EXPANSION_ENHANCEMENT = enhancements.getPreExpansionBool();
			POST_EXPANSION_ENHANCEMENT = enhancements.getPostExpansionBool();
			PRE_SIMULATION_ENHANCEMENT = enhancements.getPreSimulationBool();
			POST_SIMULATION_ENHANCEMENT = enhancements.getPostSimulationBool();
			PRE_BACKPROP_ENHANCEMENT = enhancements.getPreBackpropagationBool();
			POST_BACKPROP_ENHANCEMENT = enhancements.getPostBackpropagationBool();
		}
	}
}
