package za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MctsNodeExtensionParallelInterface;

import java.util.Hashtable;
import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.CopyOnWriteArrayList;

public final class MctsNodeTreeParallel<S extends GameState> implements MctsNodeTreeParallelInterface<S, MctsNodeTreeParallel<S>, MctsNodeTreeParallel<S>> {

	private final MctsNodeComposition<S, MctsNodeTreeParallel<S>, MctsNodeTreeParallel<S>> mctsBasic;
	private double virtualLoss;
	protected int depth;

	// this node
	private final ReadWriteLock lock = new ReentrantReadWriteLock();
	private final Lock writeLock = lock.writeLock();
	private final Lock readLock = lock.readLock();

	// lock for value
    private final ReadWriteLock lockValue = new ReentrantReadWriteLock();
    private final Lock writeLockValue = lockValue.writeLock();
    private final Lock readLockValue = lockValue.readLock();

    // lock for Parent
    private final ReadWriteLock lockParent = new ReentrantReadWriteLock();
    private final Lock writeLockParent = lockParent.writeLock();
    private final Lock readLockParent = lockParent.readLock();

    // lock for Children
    private final ReadWriteLock lockChildren = new ReentrantReadWriteLock();
    private final Lock writeLockChildren = lockChildren.writeLock();
    private final Lock readLockChildren = lockChildren.readLock();

    // lock for VisitCount
    private final ReadWriteLock lockVisitCount = new ReentrantReadWriteLock();
    private final Lock writeLockVisitCount = lockVisitCount.writeLock();
    private final Lock readLockVisitCount = lockVisitCount.readLock();

    // lock for PrevAction
    private final ReadWriteLock lockPrevAction = new ReentrantReadWriteLock();
    private final Lock readLockPrevAction = lockPrevAction.readLock();

    // lock for VirtualLoss
    private final ReadWriteLock lockVirtualLoss = new ReentrantReadWriteLock();
    private final Lock writeLockVirtualLoss = lockVirtualLoss.writeLock();
    private final Lock readLockVirtualLoss = lockVirtualLoss.readLock();

	// lock for State
	private final ReadWriteLock lockState = new ReentrantReadWriteLock();
	private final Lock readLockState = lockState.readLock();

	// lock for Depth
	private final ReadWriteLock lockDepth = new ReentrantReadWriteLock();
	private final Lock writeLockDepth = lockDepth.writeLock();
	private final Lock readLockDepth = lockDepth.readLock();

	// lock for enhancement classes
	private final ReadWriteLock enhancementClasses = new ReentrantReadWriteLock();
	private final Lock readLockEnhancementClasses = enhancementClasses.readLock();

	/**
	 * Constructor used by the new structure
	 * @param state the board state for this node
	 * @param prevAction the action most recently made to get to this board state
	 * @param parent the node which made the prevAction
	 * @param children nodes each relating to a unique possible action from the current board state
	 * @param logic the logic rules of the game object
	 */
	public MctsNodeTreeParallel(S state, Action prevAction, MctsNodeTreeParallel<S> parent, List<MctsNodeTreeParallel<S>> children, GameLogic<S> logic, Hashtable<String, MctsNodeExtensionParallelInterface> enhancementClasses, int playerID) {
		if (children == null) {
			children = new ArrayList<>();
		}
		mctsBasic = new MctsNodeComposition<>(state, prevAction, parent, children, logic, this::getAnUnexploredChild, this::getChildVisitCount, this::getChildValue, this::getSelfAsChildType, this::incChildVisitCount, true, enhancementClasses, playerID);
		virtualLoss = 0;
		for (MctsNodeExtensionParallelInterface extension: enhancementClasses.values()) {
			extension.setUp(mctsBasic);
		}
		if (parent != null) {
			this.depth = parent.getDepth()+1;
		} else {
			this.depth = 0;
		}
	}

	/**
	 * Constructor used by the old structure
	 * @param state the board state for this node
	 * @param prevAction the action most recently made to get to this board state
	 * @param parent the node which made the prevAction
	 * @param children nodes each relating to a unique possible action from the current board state
	 * @param logic the logic rules of the game object
	 */
	public MctsNodeTreeParallel(S state, Action prevAction, MctsNodeTreeParallel<S> parent, List<MctsNodeTreeParallel<S>> children, GameLogic<S> logic) {
		if (children == null) {
			children = new ArrayList<>();
		}
		mctsBasic = new MctsNodeComposition<>(state, prevAction, parent, children, logic, this::getAnUnexploredChild, this::getChildVisitCount, this::getChildValue, this::getSelfAsChildType, this::incChildVisitCount, true);
		virtualLoss = 0;

		if (parent != null) {
			this.depth = parent.getDepth()+1;
		} else {
			this.depth = 0;
		}
	}

	/**
	 * locks read usage of the enhancement classes array
	 */
	public void readLockEnhancementClassesArrayList() {
		readLockEnhancementClasses.lock();
	}

	/**
	 * unlocks read usage of the enhancement classes array
	 */
	public void readUnlockEnhancementClassesArrayList() {
		readLockEnhancementClasses.unlock();
	}

	/**
	 * @return the enhancement classes array for this node.
	 */
	public Hashtable<String, MctsNodeExtensionParallelInterface> getEnhancementClasses() {
		readLockEnhancementClasses.lock();
		try{
			return mctsBasic.getEnhancementClasses();
		} finally {
			readLockEnhancementClasses.unlock();
		}
	}

	/**
	 * @return the player ID of the player to play next from this node
	 */
	public int getPlayerID() {
		return mctsBasic.getPlayerID();
	}



	/**Prints to log the win to visit rate of all children as well as the value attributed to that move according
	 * to the relevant implementation (This value would be used during selection to choose an action) */
	public void logPossibleMoves() {
		Log.debug("Possible moves values:");
		for(MctsNodeTreeParallel<S> node : this.getChildren()) {
			MctsNodeTreeParallel child = node;
			Log.debug("MCTS", child.getPrevAction().toString() + "  Vanilla mcts winrate: " + child.getValue() + "/"
					+ child.getVisitCount() + " (" + Math.round(100*child.getValue()/child.getVisitCount() * 100.0) / 100.0  + "%)");
			Hashtable<String, MctsNodeExtensionParallelInterface> enhancements = this.getEnhancementClasses();
			for (MctsNodeExtensionParallelInterface extension: enhancements.values()) {
				extension.logPossibleMoves(child);
			}

		}
	}

	/**
	 * @return Depth of this node in the search TreeEngine. 0 is the depth of the TreeEngine root.
	 */
	public int getDepth() {
		readLockDepth.lock();
		try {
			return depth;
		} finally {
			readLockDepth.unlock();
		}
	}

	/**
	 * @return the state relating to the board state at this node
	 */
	public S getState() {
		readLockState.lock();
		try {
			return mctsBasic.getState();
		} finally {
			readLockState.unlock();
		}
	}

	/**
	 * @return the number of winning playouts played through this node
	 */
	public double getValue() {
		readLockValue.lock();
		try {
			return mctsBasic.getValue();
		} finally {
			readLockValue.unlock();
		}
	}

	/**
	 * increments the number of winning playouts played through this node field
	 * @param add the value to increment
	 */
	public void addValue(double add) {
        writeLockValue.lock();
     	try {
			mctsBasic.addValue(add);
		} finally {
			writeLockValue.unlock();
		}
	}

	/**
	 * @return a string representation of this node's state
	 */
	public String toString() {
		String s = "toString MctsNodeTreeParallel: try failed";
		readLock.lock();
		try {
			String parentString = (this.getParent() == null) ? "null" : this.getParent().getState().toString();
			s = "MctsNodeTreeParallel:\n\tState = " + getState().toString() + "\n\tvalue = " + getValue() +
			    "\n\tParent (state) = " + parentString + "\n\tChildrenSize = " + getChildren().size() +
			    "\n\tvisitCount = " + getVisitCount() + "\n\tUnexploredChildren = " + getUnexploredChildren();
		} finally {
			readLock.unlock();
		}
		return s;
	}

	/**
	 * @return this node's parent node
	 */
	public MctsNodeTreeParallel<S> getParent() {
		readLockParent.lock();
		try {
			return mctsBasic.getParent();
		} finally {
			readLockParent.unlock();
		}
	}

	/**
	 * @param parent this node's parent node
	 */
	public void setParent(MctsNodeTreeParallel<S> parent) {
		writeLockParent.lock();
		try {
			mctsBasic.setParent(parent);
		} finally {
			writeLockParent.unlock();
		}
	}
	
	/**
	 * returns a thread-safe list, since the children argument was passes as a CopyOnWriteArrayList when mctsBasic was initialised.
	 * The list returned may be iterated without further locks in the calling class after having received the list.
	 * @return the list of explored children relating to the current node
	 */
	public List<MctsNodeTreeParallel<S>> getChildren() {
		readLockChildren.lock();
		try {
			return mctsBasic.getChildren();
		} finally {
			readLockChildren.unlock();
		}
	}

	/**
	 * Sets the list of explored children for this node
	 * @param children the set of explored children
	 */
	public void setChildren(List<MctsNodeTreeParallel<S>> children) {
		writeLockChildren.lock();
		try {
			mctsBasic.setChildren(new CopyOnWriteArrayList(children));
		} finally {
			writeLockChildren.unlock();
		}
	}
	
	/**
	 * thread-safe because children is a CopyOnWriteArrayList
	 * @param child the child to add to the list of explored children
	 */
	public void addChild(MctsNodeTreeParallel<S> child) {
		/* the write lock is not needed here, since the children list is 
		   thread-safe, so addChild method does not need to block other reading 
		   methods. However, the read lock is used here since the children 
		   list itself may be replaced with a different list by the method 
		   setChildren(), by locking the readlock, it will block setChildren() 
		   from writing whilst also still allowing reading threads to read.
		*/
		readLockChildren.lock();
		try {
			mctsBasic.addChild(child);

			for (MctsNodeExtensionParallelInterface extension: this.getEnhancementClasses().values()) {
				extension.addChild(child);
			}

		} finally {
			readLockChildren.unlock();
		}
	}

	/**
	 * Adds the new children to the set of explored children relating to this ndoe
	 * @param newChildren the newly explored children to be added
	 */
	public void addChildren(List<MctsNodeTreeParallel<S>> newChildren) {
		// same reasoning as addChild() for reason about read lock
		readLockChildren.lock();
		try {
			mctsBasic.addChildren(newChildren);

			for (MctsNodeExtensionParallelInterface extension: this.getEnhancementClasses().values()) {
				extension.addChildren(newChildren);
			}

		} finally {
			readLockChildren.unlock();
		}
	}

	/**
	 * @return the number of playouts made through this ndoe
	 */
	public double getVisitCount() {
		readLockVisitCount.lock();
		try {
			return mctsBasic.getVisitCount();
		} finally {
			readLockVisitCount.unlock();
		}
	}

	/**
	 * Increments the number of playouts made through this node by 1
	 */
	public void incVisitCount() {
        writeLockVisitCount.lock();
      try {
			mctsBasic.incVisitCount();
		} finally {
			writeLockVisitCount.unlock();
		}
	}

	/**
	 * Decrements the number of playouts made through this node by 1
	 */
	public void decVisitCount() {
		writeLockVisitCount.lock();
		try {
			mctsBasic.decVisitCount();
		} finally {
			writeLockVisitCount.unlock();
		}
	}

	/**
	 * Increments the number of playouts field of the child
	 * @param child
	 */
	public void incChildVisitCount(MctsNodeTreeParallel<S> child) {
		/* child of MctsNodeTreeParrallel is implemented as MctsNodeTreeParrallel,
		   since the incVisitCount() method is thread safe, no locking is required
		   for child.incVisitCount().
		*/
		child.incVisitCount();
	}

	/**
	 * @param child
	 * @return the number of playouts made through the child node
	 */
	public double getChildVisitCount(MctsNodeTreeParallel<S> child) {
		/* child of MctsNodeTreeParrallel is implemented as MctsNodeTreeParrallel,
		   since the getVisitCount() method is thread safe, no locking is required
		   for child.getVisitCount().
		*/
		return child.getVisitCount();
	}

	/**
	 * @param child
	 * @return the number of winning playouts made through the child node
	 */
	public double getChildValue(MctsNodeTreeParallel<S> child) {
		/* child of MctsNodeTreeParrallel is implemented as MctsNodeTreeParrallel,
		   since the getValue() method is thread safe, no locking is required
		   for child.getValue().
		*/
		return child.getValue();
	}
	
	/**
	 * @return concurrent thread-safe list (CopyOnWriteOnArrayList) of actions relating to unexpanded children
	 * for this node
	 */
	public List<Action> getUnexploredChildren() {
		readLockChildren.lock();
		try {
			return mctsBasic.getUnexploredChildren();
		} finally {
			readLockChildren.unlock();
		}
	}

	/**
	 * @return true if this node has explanded all it's children, false otherwise
	 */
	public boolean isUnexploredEmpty() {
		readLockChildren.lock();
		try {
			return mctsBasic.isUnexploredEmpty();
		} finally {
			readLockChildren.unlock();
		}
	}

	/**
	 * @param logic
	 * @return an unexplored child chosen by a random distribution over the list of unexplored children actions.
	 */
	public MctsNodeTreeParallel<S> getAnUnexploredChild(GameLogic<S> logic) {
		List<Action> unexplored;
		Action unexploredAction;
		S childState;
		writeLockChildren.lock();
		try {
			if (this.isUnexploredEmpty()) {
				return null;
			}
			childState = this.getState();
			unexplored = this.getUnexploredChildren();
			unexploredAction = unexplored.remove((int) (Math.random() * unexplored.size()));
			logic.makeMove(childState, unexploredAction);
		} finally {
			writeLockChildren.unlock();
		}

		Hashtable<String, MctsNodeExtensionParallelInterface> enhancementClasses = this.getEnhancementClasses();
		Hashtable<String, MctsNodeExtensionParallelInterface> newEnhancementClasses = new Hashtable<>();

		for (MctsNodeExtensionParallelInterface enhancementExtension: enhancementClasses.values()) {
			try {
				newEnhancementClasses.put(enhancementExtension.getID(), enhancementExtension.getClass().newInstance());
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		int alteratePlayer = alternatePlayer(this.getPlayerID());
		return new MctsNodeTreeParallel<>(childState, unexploredAction, this, new ArrayList<>(), logic, newEnhancementClasses, alteratePlayer);
	}

	/**
	 * @param logic
	 * @param index the index in the array
	 * @return an unexplored child chosen by indexing into the array list of unexplored children actions
	 */
	public MctsNodeTreeParallel<S> getAnUnexploredChild(GameLogic<S> logic, int index) {
		List<Action> unexplored;
		Action unexploredAction;
		S childState;
		writeLockChildren.lock();
		try {
			if (this.isUnexploredEmpty()) {
				return null;
			}
			childState = this.getState();
			unexplored = this.getUnexploredChildren();
			unexploredAction = unexplored.remove(index);
			logic.makeMove(childState, unexploredAction);
		} finally {
			writeLockChildren.unlock();
		}

		Hashtable<String, MctsNodeExtensionParallelInterface> enhancementClasses = this.getEnhancementClasses();
		Hashtable<String, MctsNodeExtensionParallelInterface> newEnhancementClasses = new Hashtable<>();

		for (MctsNodeExtensionParallelInterface enhancementExtension: enhancementClasses.values()) {
			try {
				newEnhancementClasses.put(enhancementExtension.getID(), enhancementExtension.getClass().newInstance());
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		int alteratePlayer = alternatePlayer(this.getPlayerID());
		return new MctsNodeTreeParallel<S>(childState, unexploredAction, this, new ArrayList<MctsNodeTreeParallel<S>>(), logic, newEnhancementClasses, alteratePlayer);
	}

	/**
	 * @param player
	 * @return the id of the opponent of the player given in the parameter
	 */
	public int alternatePlayer(int player) {
		if (player == 0) {
			return 1;
		} else if (player == 1) {
			return 0;
		} else {
			System.out.println("Shouldn't get here, MctsNodeTreeParallel invalid player");
			return -1;
		}
	}

	/**
	 * @return the action most recently made to get to this node's board state
	 */
	public Action getPrevAction() {
		readLockPrevAction.lock();
		try {
			return mctsBasic.getPrevAction();
		} finally {
			readLockPrevAction.unlock();
		}
	}

	/**
	 * @return the current node as a child type object
	 */
	public MctsNodeTreeParallel<S> getSelfAsChildType() {
		readLock.lock();
		try {
			return this;
		} finally {
			readLock.unlock();
		}
	}

	/**
	 * @return the current value of virtual loss applied at this node
	 */
	public double getVirtualLoss() {
		readLockVirtualLoss.lock();
		try {
			return this.virtualLoss;
		} finally {
			readLockVirtualLoss.unlock();
		}
	}

	/**
	 * decrement the number of winning playouts made through this node by the loss value
	 * @param lossValue
	 */
	public void applyVirtualLoss(double lossValue) {
		writeLockVirtualLoss.lock();
		try {
			this.virtualLoss -= lossValue;
			this.addValue(lossValue);
		} finally {
			writeLockVirtualLoss.unlock();
		}
	}

	/**
	 * increment the number of winning playouts made through this node by the loss value
	 * @param lossValue
	 */
	public void restoreVirtualLoss(double lossValue) {
		writeLockVirtualLoss.lock();
		try {
			this.virtualLoss += lossValue;
			this.addValue(-lossValue);
		} finally {
			writeLockVirtualLoss.unlock();
		}
	}

	/**
	 * Lock this node with a read lock
	 */
	public void readLock() {
		this.readLock.lock();	
	}

	/**
	 * Unlock this node with a read lock
	 */
	public void readUnlock() {
		this.readLock.unlock();
	}

	/**
	 * Lock this node with a write lock
	 */
	public void writeLock() {
		this.writeLock.lock();
	}

	/**
	 * Unlock this node with a write lock
	 */
	public void writeUnlock() {
		this.writeLock.unlock();
	}
	
}
