package za.ac.sun.cs.ingenious.games.cardGames.hearts.engines;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;
import java.util.Map;
import java.util.Random;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.games.cardGames.classicalDeck.ClassicalSuits;
import za.ac.sun.cs.ingenious.games.cardGames.classicalDeck.ClassicalSymbols;
import za.ac.sun.cs.ingenious.games.cardGames.core.actions.PlayCardAction;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.KeyNotFoundException;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.LocationNotFoundException;
import za.ac.sun.cs.ingenious.games.cardGames.core.rack.CardRack;
import za.ac.sun.cs.ingenious.games.cardGames.hearts.game.HeartsGameLogic;
import za.ac.sun.cs.ingenious.games.cardGames.hearts.game.HeartsGameState;
import za.ac.sun.cs.ingenious.games.cardGames.hearts.game.HeartsInitGameMessage;
import za.ac.sun.cs.ingenious.games.cardGames.hearts.game.HeartsLocations;

/**
 * A rule based Hearts engine.
 * Can be used as a reference player when implementing other players - basic level.
 * 
 * @author Joshua Wiebe
 */
public class HeartsSimpleRuleBasedEngine extends Engine {

	/** A HeartsGameState for internal representation. */
	HeartsGameState state;

	/** A HeartsGameLogic for internal representation. */
	HeartsGameLogic logic;

	/**
	 * Instantiates a new Hearts engine.
	 *
	 * @param toServer
	 *            the server connection
	 */
	public HeartsSimpleRuleBasedEngine(EngineToServerConnection toServer) {
		super(toServer);
		logic = new HeartsGameLogic();
	}

	@Override
	public void setZobrist(ZobristHashing zobristHashing) {

	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.Engine#engineName()
	 * 
	 * @return Returns it's name.
	 */
	@Override
	public String engineName() {
		return "HeartsSimplePlayer";
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.Engine#receiveInitAction(za.ac.sun.cs.ingenious.core.network.game.actions.InitGameAction)
	 * 
	 *      Initialize the game state and update it with the given CardRack.
	 * 
	 */
	@Override
	public void receiveInitGameMessage(InitGameMessage a) {
		
		// Cast InitGameAction to HeartsInitGameAction
		HeartsInitGameMessage higa = (HeartsInitGameMessage) a;

		// Retrieve CardRack
		CardRack<ClassicalSymbols, ClassicalSuits> rack = higa.getRack();

		// Initialize the game state with a Hearts deck.
		state = new HeartsGameState(higa.getNumPlayers());

		// Update game state according to given rack.
		Map<Card<ClassicalSymbols, ClassicalSuits>, ArrayList<HeartsLocations>> map = state.getMap();
		for (Map.Entry<Card<ClassicalSymbols, ClassicalSuits>, ArrayList<HeartsLocations>> entry : map.entrySet()) {
			Card<ClassicalSymbols, ClassicalSuits> card = entry.getKey();
			
			// If card is on initial rack of this player: change to player location.
			if (rack.contains(card)) {
				try {
					state.changeLocation(card, HeartsLocations.DRAWPILE, HeartsLocations.values()[super.playerID]);
				} catch (KeyNotFoundException | LocationNotFoundException e) {
					e.printStackTrace();
				}
			}
			
			// Else: change location to UNKNOWNPLAYER
			else {
				try {
					state.changeLocation(card, entry.getValue().get(0), HeartsLocations.UNKNOWNPLAYER);
				} catch (KeyNotFoundException | LocationNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.Engine#receivePlayMove(za.ac.sun.cs.ingenious.core.network.game.actions.PlayMoveAction)
	 * 
	 *      Update internal state according to received PlayMoveAction.
	 * 
	 *      Cannot simply use makeMove() of HeartsGameLogic because of imperfect
	 *      information.
	 * 
	 */
	@Override
	public void receivePlayedMoveMessage(PlayedMoveMessage m) {
		Move move = m.getMove();

		PlayCardAction<HeartsLocations> pcMove = (PlayCardAction) move;

		// Update current player of state to make use of makeMove. (Need to trust the referee)
		state.setCurrentPlayer(pcMove.getPlayerID());
		
		// This player
		if (pcMove.getPlayerID() == this.getPlayerID()) {
			logic.makeMove(state, pcMove);
		}

		// Other player
		else {
			try {
				// Update state
				state.changeLocation(pcMove.getCard(), HeartsLocations.UNKNOWNPLAYER, HeartsLocations.values()[pcMove.getPlayerID()]);
				logic.makeMove(state, pcMove);
			} catch (KeyNotFoundException | LocationNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.Engine#receiveGenerateMove(za.ac.sun.cs.ingenious.core.network.game.actions.GenMoveAction)
	 * 
	 * Generate move.
	 * 
	 */
	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		// Fetch all possible moves.
		ArrayList<Action> actions = (ArrayList<Action>) logic.generateActions(state, this.playerID);
		
		// If no possible moves (should never be the case).
		if (actions.size() == 0) {
			return null;
		} 
		
		Action selectedAction = null;
		
		// If only one move is possible: send this move. 
		if(actions.size() == 1){
			return new PlayActionMessage(actions.get(0));
		}
		
		// If trick pile is empty (first player in the round): play lowest possible card.
		if(state.getCurrentTrick().isEmpty()){
			
			// Fetch first action.
			Action actionWithLowestCard = actions.get(0);
			
			// Extract first actions card.
			Card<ClassicalSymbols, ClassicalSuits> lowestCard = ((PlayCardAction) actions.get(0)).getCard();
			
			for(Action action : actions){
				
				// Extract card from current action
				Card<ClassicalSymbols, ClassicalSuits> currentCard = ((PlayCardAction) action).getCard();
				
				// If symbol is lower then symbol of current lowest card: this is the new lowest card.
				if(currentCard.getf2().ordinal() > lowestCard.getf2().ordinal()){
					lowestCard = ((PlayCardAction) action).getCard();
					actionWithLowestCard = action;
				}
			}
			return new PlayActionMessage(actionWithLowestCard);
		}
		
		// If trick is not empty:
		else{
			// Fetch current highest card of trick
			Card<ClassicalSymbols, ClassicalSuits> highestCardOfTrick = state.getHighestCardOfTrick();
			
			// Action with highest possible card without getting the trick.
			Action highestPossibleAction = null;
			
			// Helper to gather all actions which are of the same suit but higher than current highest card.
			ArrayList<Action> badActions = new ArrayList<>();
			
			for(Action action : actions){
				
				// Extract card from current action
				Card<ClassicalSymbols, ClassicalSuits> currentCard = ((PlayCardAction) action).getCard();
				
				// If symbol is lower then symbol of current lowest card: this is the new lowest card.
				if(currentCard.getf2().equals(state.getCurrentTrickSuit()) && currentCard.getf1().ordinal() < highestCardOfTrick.getf1().ordinal()){
					
					// If no suitable action found so far: set action to current action.
					if(highestPossibleAction == null){
						highestPossibleAction = action;
					} 
					
					// Else check if this action is more suitable than the best one found so far (i. e. card is higher but still without getting the trick).
					else if( ((ClassicalSymbols) ((PlayCardAction) highestPossibleAction).getCard().getf1()).ordinal() < currentCard.getf1().ordinal()){
						highestPossibleAction = action;
					}
				}
				
				// Else if the card of the action is of the current trick suit but not lower: store for later processing
				else if(currentCard.getf2().equals(state.getCurrentTrickSuit())){
					badActions.add(action);
				}
			}
			
			// If best action found without getting the current trick (so far): return it.
			if(highestPossibleAction != null){
				return new PlayActionMessage(highestPossibleAction);
			}

			// If no action found without getting the current trick (so far) and suitable card on hand: play lowest card possible
			else if(!badActions.isEmpty()){
				
				// If not last one to act in current round: play lowest card possible
				if(state.getCurrentTrick().size() != state.getNumPlayers()-1){
					// Fetch first action.
					Action actionWithLowestCard = badActions.get(0);
					
					// Extract first actions card.
					Card<ClassicalSymbols, ClassicalSuits> lowestCard = ((PlayCardAction) actionWithLowestCard).getCard();
					
					for (Action action : badActions) {
						
						// Extract card from current action
						Card<ClassicalSymbols, ClassicalSuits> currentCard = ((PlayCardAction) action).getCard();

						// If symbol is lower then symbol of current lowest card:
						// this is the new lowest card.
						if (currentCard.getf2().ordinal() < lowestCard.getf2().ordinal()) {
							lowestCard = ((PlayCardAction) action).getCard();
							actionWithLowestCard = action;
						}
					}
					
					// Return best action.
					 return new PlayActionMessage(actionWithLowestCard);
				}
				
				// If last one to act in current round: play highest card possible (but not Q of spades):
				else if(state.getCurrentTrick().size() == state.getNumPlayers()-1){
					// Fetch first action.
					Action actionWithHighestCard = badActions.get(0);
					
					// Extract first actions card.
					Card<ClassicalSymbols, ClassicalSuits> highestCard = ((PlayCardAction) actionWithHighestCard).getCard();
					
					for (Action action : badActions) {
						// Extract card from current action
						Card<ClassicalSymbols, ClassicalSuits> currentCard = ((PlayCardAction) action).getCard();
						// If symbol is higher then symbol of current highest card:
						if (currentCard.getf2().ordinal() > highestCard.getf2().ordinal()) {
							// This is the new highest card. But avoid Q of spades
							if(! currentCard.equals(new Card<>(ClassicalSymbols.QUEEN, ClassicalSuits.SPADES))){
								highestCard = ((PlayCardAction) action).getCard();
								actionWithHighestCard = action;
							}
						}
					}
					
					// Return best action.
					return new PlayActionMessage(actionWithHighestCard);
				}
			}
			
			// If player can't serve current trick suit: play worst card.
			else if(badActions.isEmpty()){
				
				// Placeholder for worst card.
				Action actionWithHighestCard = actions.get(0);
				
				// Extract first actions card.
				Card<ClassicalSymbols, ClassicalSuits> highestCard = ((PlayCardAction) actionWithHighestCard).getCard();
				
				for (Action action : badActions) {
					
					// Extract card from current action
					Card<ClassicalSymbols, ClassicalSuits> currentCard = ((PlayCardAction) action).getCard();
					
					if(currentCard.equals(new Card<>(ClassicalSymbols.QUEEN, ClassicalSuits.SPADES))){
						highestCard = ((PlayCardAction) action).getCard();
						actionWithHighestCard = action;
						break;
					}

					// If symbol is higher then symbol of current highest card:
					// this is the new highest card.
					if (currentCard.getf2().ordinal() > highestCard.getf2().ordinal()) {
						
						// Prefer playing hearts.
						if(!highestCard.getf2().equals(ClassicalSuits.HEARTS) || currentCard.getf2().equals(ClassicalSuits.HEARTS)){
							highestCard = ((PlayCardAction) action).getCard();
							actionWithHighestCard = action;
						}
					}
				}
				
				return new PlayActionMessage(actionWithHighestCard);
			}
		}
		
		return null;
	}

	/**
	 * @see za.ac.sun.cs.ingenious.core.Engine#receiveGameTerminatedAction(za.ac.sun.cs.ingenious.core.network.game.actions.GameTerminatedAction)
	 */
	@Override
	public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
	}

	/**
	 * Print the current rack sizes of the state.
	 */
	public void printRackSizes() {
		StringBuilder s = new StringBuilder();
		s.append("Round: " + state.getRoundNR() + "\n");
		s.append("Racks: [");
		for (int i = 0; i < state.getNumPlayers(); i++) {
			if (i != 0) {
				s.append(", ");
			}
			s.append(state.getCopyOfNrTricksOfPlayers()[i]);
		}
		s.append("] \n");

		Log.info(s);
	}
}
