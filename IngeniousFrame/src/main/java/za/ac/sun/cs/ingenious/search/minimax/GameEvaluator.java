package za.ac.sun.cs.ingenious.search.minimax;

import za.ac.sun.cs.ingenious.core.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.GameState;

/**
 * Used to evaluate the state of a game that has not yet ended.
 * Every game running MiniMax search most supply a class implementing this.
 * The methods are the same as in {@link GameFinalEvaluator}, but the getScore method must
 * also work for non-terminal state.
 * @author Michael Krause
 */
public interface GameEvaluator<S extends GameState> extends GameFinalEvaluator<S> {

}
