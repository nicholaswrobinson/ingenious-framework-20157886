package za.ac.sun.cs.ingenious.core.network;

import java.io.Serializable;


/**
 * Super class for all messages being transfered between server and client.
 * @author Stephan Tietz
 */
public class Message implements Serializable {

	private static final long serialVersionUID = 1L;
	
}
