package za.ac.sun.cs.ingenious.games.mnk;

import com.esotericsoftware.minlog.Log;

import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.util.message.MatchSettingsInitGameMessage;
import za.ac.sun.cs.ingenious.core.util.referee.GeneralReferee;
	
public class MNKReferee extends GeneralReferee<MNKState, MNKLogic, MNKFinalEvaluator> {
	
	//Constructor
	public MNKReferee(MatchSetting match, PlayerRepresentation[] players) throws MissingSettingException, IncorrectSettingTypeException{
		super(match,
				players, new MNKState(match.getSettingAsInt("mnk_height"), match.getSettingAsInt("mnk_width"), match.getSettingAsInt("mnk_k"),
						match.getSettingAsBoolean("perfectInformation"), match.getNumPlayers()),
				new MNKLogic(),
				new MNKFinalEvaluator(), new MNKLogic());
	}
	
	//Just printing the core game settings of MNK
	@Override
	protected void beforeGameStarts(){
		StringBuilder s = new StringBuilder();
		s.append("\nGame Settings:\n");
		try {
			s.append("Height: " + this.matchSettings.getSettingAsInt("mnk_height") + "\n");
			s.append("Width: " + this.matchSettings.getSettingAsInt("mnk_width") + "\n");
			s.append("K: " + this.matchSettings.getSettingAsInt("mnk_k") + "\n");
			s.append("Perfect information: " + this.matchSettings.getSettingAsBoolean("perfectInformation") + "\n");
		} catch (MissingSettingException | IncorrectSettingTypeException e) {
			Log.error("Could not read settings from received MatchSetting object, message: " + e.getMessage());
		}
		
		Log.info(s);
	}
	
	@Override
	protected InitGameMessage createInitGameMessage(PlayerRepresentation player) {
		return new MatchSettingsInitGameMessage(matchSettings);
	}
}
