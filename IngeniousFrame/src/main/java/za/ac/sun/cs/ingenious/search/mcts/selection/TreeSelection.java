package za.ac.sun.cs.ingenious.search.mcts.selection;

import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;

/**
 * The TreeEngine selection strategy that determines how the TreeEngine is traversed from
 * the root node down in the TreeEngine parallelisation MCTS version. The selection
 * strategy should be mindful about the balance between exploration and 
 * exploitation. A standard implementation of this strategy is 
 * {@link TreeSelectionUct}.
 *
 * @author Karen Laubscher
 * 
 * @param <N> The mcts node type
 */
public interface TreeSelection<N extends MctsNodeTreeParallelInterface<? , N, ?>> {

	/**
	 * The method that decide which child node to traverse to next. Since the 
	 * TreeEngine structure is shared, nodes should be (read) locked when viewing
	 * information used in calculations.
	 * 
	 * @param node  The current node whose children nodes are considered for the
	 *				next node to traverse to.
	 * 
	 * @return      The child node that is selected.
	 */
	N select(N node);

}
