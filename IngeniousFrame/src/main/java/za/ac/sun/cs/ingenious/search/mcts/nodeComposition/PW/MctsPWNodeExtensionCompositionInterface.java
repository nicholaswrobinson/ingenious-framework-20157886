package za.ac.sun.cs.ingenious.search.mcts.nodeComposition.PW;

import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeComposition;

public interface MctsPWNodeExtensionCompositionInterface {

    String getID();

    void setUp(MctsNodeComposition node);

}