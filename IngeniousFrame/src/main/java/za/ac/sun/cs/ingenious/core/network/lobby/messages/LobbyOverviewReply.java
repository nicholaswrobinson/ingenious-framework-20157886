package za.ac.sun.cs.ingenious.core.network.lobby.messages;

import za.ac.sun.cs.ingenious.core.network.Message;

/**
 * Message contains information about all currently open lobbies.
 * @author Stephan Tietz
 */
public class LobbyOverviewReply extends Message {

	private static final long serialVersionUID = 1L;
	
	String[] lobbyNames;
	String[] gameNames;
	int[] numPlayers;
	int[] maxPlayers;
	
	
	public LobbyOverviewReply(String[] lobbyNames, String[] gameNames, int[] numPlayers, int[] maxPlayers) {
		this.lobbyNames = lobbyNames;
		this.gameNames = gameNames;
		this.numPlayers = numPlayers;
		this.maxPlayers = maxPlayers;
	}
	
	
	public String[] getGameNames() {
		return gameNames;
	}
	 
	public String[] getLobbyNames() {
		return lobbyNames;
	}
	
	public int[] getMaxPlayers() {
		return maxPlayers;
	}
	
	public int[] getNumPlayers() {
		return numPlayers;
	}
	
}
