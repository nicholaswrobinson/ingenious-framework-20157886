package za.ac.sun.cs.ingenious.games.ingenious.search.mcts;

import java.util.ArrayList;

import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousAction;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousBoard;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousRack;
import za.ac.sun.cs.ingenious.games.ingenious.Tile;
import za.ac.sun.cs.ingenious.search.mcts.legacy.SearchNode;

public class SimpleIngeniousNode {
	public int numberOfVisits;
	public double numberOfWins;
	private SimpleIngeniousNode parent = null;
	protected ArrayList<SimpleIngeniousNode> expandedChildren = new ArrayList<SimpleIngeniousNode>();
	protected ArrayList<IngeniousAction> unExpandedMoves = new ArrayList<IngeniousAction>();
	protected final IngeniousBoard gameBoard;
	protected short nextPlayer;
	private short currentPlayer;
	private IngeniousAction originMove;
	protected ArrayList<IngeniousRack> rack;
	protected ArrayList<Tile> bag;
	private int depth;
	private int bagSkip = 0;

	public SimpleIngeniousNode(IngeniousBoard board, IngeniousAction move, short player,
			ArrayList<IngeniousRack> rack, ArrayList<Tile> bag, SimpleIngeniousNode parent) {
		this.parent = parent;
		for (IngeniousRack r : rack) {
			bagSkip += r.size();
		}
		if (parent != null) {
			depth = this.parent.depth + 1;
		} else {
			depth = 0;
		}
		this.rack = rack;
		this.bag = bag;
		this.gameBoard = board.copy();

		this.originMove = move;
		numberOfVisits = 0;
		numberOfWins = 0;
		currentPlayer = player;
		if (player == 1) {
			nextPlayer = 2;
		} else {
			nextPlayer = 1;
		}
		if (originMove != null) {
			gameBoard.makeMove(originMove); //TODO bug? called again in getMoves()
		}
		unExpandedMoves = this.getMoves();
	}

	private ArrayList<IngeniousAction> getMoves() {
		if (this.originMove != null) {
			gameBoard.makeMove(this.originMove);
			ArrayList<IngeniousAction> moves = gameBoard.generateMoves(-1,rack
					.get(currentPlayer-1));
			gameBoard.undoMove();
			return moves;
		} else {
			return gameBoard.generateMoves(-1,rack.get(currentPlayer-1));
		}

	}

	public ArrayList<SimpleIngeniousNode> getExpandedChildren() {
		return expandedChildren;
	}

	public long getZobristHash() {
		// TODO Auto-generated method stub
		return gameBoard.hashCode();
	}

	public void setVisits(int visits) {
		numberOfVisits = visits;
	}

	public int getVisits() {
		// TODO Auto-generated method stub
		return numberOfVisits;
	}

	public void setWins(int wins) {
		numberOfWins = wins;
	}

	public double[] getReward() {
		// TODO Auto-generated method stub
		double[] res = new double[1];
		res[0] = numberOfWins; 
		return res;
	}

	public boolean expandable() {

		return unExpandedMoves.size() > 0 && (!gameBoard.full());
	}

	public IngeniousRack getRack() {
		// TODO Auto-generated method stub
		return this.rack.get(currentPlayer);
	}

	public Move getMove() {
		// TODO Auto-generated method stub
		return this.originMove;
	}

//	public IngeniousMinMaxBoardInterface getGameState() {
//		// TODO Auto-generated method stub
//		return this.gameBoard.copy();
//	}

	public SimpleIngeniousNode expandAChild() {
		int index = (int) (Math.random() * unExpandedMoves.size());
		IngeniousAction randomChildMove = unExpandedMoves.remove(index);
		rack.get(currentPlayer - 1).remove(randomChildMove.getTile());
		if (bagSkip + depth -1 < bag.size()) {
			rack.get(currentPlayer - 1).add(bag.get(bagSkip + depth-1));
		}else{
			int top = (int) (Math.random() * this.gameBoard.getNumColours());
			int bottom = (int) (Math.random() * this.gameBoard.getNumColours());
			rack.get(currentPlayer-1).add(new Tile(top,bottom));
		}
		SimpleIngeniousNode expandedNode = new SimpleIngeniousNode(gameBoard, randomChildMove,
				nextPlayer, rack, bag, this);

		expandedChildren.add(expandedNode);
		return expandedNode;
	}

	public SimpleIngeniousNode getParent() {
		// TODO Auto-generated method stub
		return this.parent;
	}

	public void addReward(double[] d) {
		this.numberOfWins = this.numberOfWins + d[0];

	}

	public int getCurrentPlayer() {
		// TODO Auto-generated method stub
		return this.currentPlayer;
	}

	public void transpositionUpdate(SearchNode node) {
		throw new UnsupportedOperationException();

	}

}
