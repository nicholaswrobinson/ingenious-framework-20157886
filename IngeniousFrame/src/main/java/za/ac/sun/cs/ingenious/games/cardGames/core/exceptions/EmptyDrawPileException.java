package za.ac.sun.cs.ingenious.games.cardGames.core.exceptions;

/**
 * The class EmptyDrawPileException.
 * 
 * Raised when the draw pile is empty.
 * 
 * @author Joshua Wiebe
 */
public class EmptyDrawPileException extends RuntimeException{

	/** The serialVersionUID. */
	private static final long serialVersionUID = 327482937428L;
	
	/**
	 * Instantiates a new EmptyDrawPileException.
	 *
	 * @param message For superclass.
	 */
	public EmptyDrawPileException(String message){
		super(message);
	}
	
}
