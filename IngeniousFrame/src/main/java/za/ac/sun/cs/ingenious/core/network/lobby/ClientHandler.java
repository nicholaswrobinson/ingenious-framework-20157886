package za.ac.sun.cs.ingenious.core.network.lobby;

import com.esotericsoftware.minlog.Log;
import com.google.common.base.Preconditions;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.network.TCPProtocol;
import za.ac.sun.cs.ingenious.core.network.lobby.messages.JoinMessage;
import za.ac.sun.cs.ingenious.core.network.lobby.messages.LobbyOverviewReply;
import za.ac.sun.cs.ingenious.core.network.lobby.messages.LobbyOverviewRequest;
import za.ac.sun.cs.ingenious.core.network.lobby.messages.NewMatchMessage;

/**
 * Used by {@link GameServer} to handle all communication with a specific client.
 * See the documentation for the run-method.
 */
public class ClientHandler implements Runnable {

    private Socket             client;
    private ObjectOutputStream outputStream;
    private ObjectInputStream  inputStream;
    private String             playerName;
    private LobbyManager       lobbyMan;
    private boolean            isJoined;
    private LobbyHost          joinedController;
    private int				   idInLobby;

    /**
     * Interrupting the {@link ClientHandler} will close streams.
     * If not marked volatile then the client thread might never see changes
     * to this variable.
     */
    private volatile boolean interrupt;

    public ClientHandler(Socket client, LobbyManager lm) {
        Preconditions.checkNotNull(client);
        Preconditions.checkNotNull(lm);
        this.client = client;
        this.lobbyMan = lm;
        this.idInLobby = -1;
    }

    public String getPlayerName() {
        return playerName;
    }

    /**
     * @return The LobbyOverViewReply message a client gets when they want a list of open lobbies.
     */
    private LobbyOverviewReply createLobbyOverview() {
        String[] lobbyNames = lobbyMan.getLobbyNames();
        String[] games = new String[lobbyNames.length];
        int[] numPlayers = new int[lobbyNames.length];
        int[] maxPlayers = new int[lobbyNames.length];

        for (int i = 0; i < lobbyNames.length; i++) {
            LobbyHost c = lobbyMan.getLobby(lobbyNames[i]);
            games[i] = c.getMatchSetting().getGameName();
            numPlayers[i] = c.getNumberOfEngines();
            maxPlayers[i] = c.getMaxPlayers();
        }
        return new LobbyOverviewReply(lobbyNames, games, numPlayers, maxPlayers);
    }

    /**
     * The run method begins with a handshake where the client must send a unique name to the ClientHandler.
     * If the name is accepted the {@link ClientHandler} will begin waiting for messages.
     * Possible messages are:
     * {@link NewMatchMessage}: will create a new match
     * {@link LobbyOverviewRequest}: will send an overview over the currently open lobbies
     * {@link JoinMessage}: will join into a specified lobby
     * For backwards compatibility reading a string array is also still supported, but not recommended.
     */
    @Override
	public void run() {
        if (!openStreams()) {
			return;
		}
        try {
        	Log.info("Waiting for player name...");
            playerName = (String) inputStream.readObject(); //TODO should be message instead

            while (lobbyMan.checkDuplicateClientHandlerName(playerName)) {
                outputStream.writeObject("NameTaken");
                playerName = (String) inputStream.readObject();
            }
            outputStream.writeObject("Acknowledged");
            Log.info("Received player name: " + playerName);
        } catch (Exception e) {
            Log.error("Could not receive player name. Closing connection.");
            closeConnection();
            return;
        }

        Log.info("Connection to client " + client.getInetAddress() + " with name " + playerName + " established");
        lobbyMan.addClientHandler(playerName, this);

        while (!interrupt) {
            try {
                Object input = inputStream.readObject();
                if (input instanceof NewMatchMessage) {
                    NewMatchMessage a = (NewMatchMessage) input;
                    MatchSetting match = a.getMatchSetting();

                    LobbyHost lh = new LobbyHost(match, lobbyMan);
                    if (lobbyMan.addLobby(match.getLobbyName(), lh)) {
	                    Log.info("Match received, created lobby " + match.getLobbyName());
	                    outputStream.writeObject(TCPProtocol.SUCCESS); 
                    } else {
	                    Log.info("Could not create lobby");
	                    outputStream.writeObject(TCPProtocol.FAILURE);                   	
                    }

                    interrupt = true;
                } else if (input instanceof LobbyOverviewRequest) {
                    outputStream.writeObject(createLobbyOverview());

                } else if (input instanceof JoinMessage) {
                    JoinMessage a = (JoinMessage) input;
                    joinedController = lobbyMan.getLobby(a.getLobbyName());
                    if (joinedController != null) {
                        idInLobby = joinedController.acceptJoiningPlayers(client, inputStream, outputStream);
                        isJoined = true;
                        interrupt = true;
                    } else {
                        Log.error("Could not join " + a.getLobbyName() + ": Lobby not found");
                    }
                } else if (input instanceof String) {
                    String[] fields = ((String) input).split(" ");
                    String command = fields[0];
                    String args = fields.length > 1 ? fields[1] : null;
                    Log.info("	received: " + input);
                    if (command.equals(TCPProtocol.MATCHSETTING)) {
                        String controllerName = (String) inputStream.readObject();
                        outputStream.writeObject(lobbyMan.getMatchSettings(controllerName));
                    } else {
                        Log.error("Recieved " + command + " - don't understand.");
                    }
                }
            } catch (IOException | ClassNotFoundException e) {
            	Log.error("Unexpected transmission or connection interrupted. Closing connection.");
                if (isJoined) {
                    joinedController.engineDisconnected(idInLobby);
                }
                interrupt = true;
            }
        }

        if (!isJoined) {
	        Log.info("Now closing connection to " + client.getInetAddress());
	        closeConnection();
        }
        lobbyMan.removeClientHandler(playerName);
    }

    public void interrupt() {
        this.interrupt = true;
    }

    /**
     * Opens IO-Streams catches and prints failures
     */
    public boolean openStreams() {
        try {
            outputStream = new ObjectOutputStream(this.client.getOutputStream());
            inputStream = new ObjectInputStream(this.client.getInputStream());
        } catch (IOException e) {
            Log.error("Input Stream failed to open.",e);
            return false;
        }
        return true;
    }

    /**
     * Disconnects IO-streams and closes socket catches and prints failures
     */
    public void closeConnection() {
        try {
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
        	Log.error("Input stream failed to close",e);
        }

        try {
            client.close();
        } catch (IOException e) {
        	Log.error("Socket failed to close",e);
        }
    }
}
