package za.ac.sun.cs.ingenious.games.bomberman.mcts;

import com.esotericsoftware.minlog.Log;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.util.misc.CompassDirection;
import za.ac.sun.cs.ingenious.core.util.move.CompassDirectionAction;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMBoard;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMFinalEvaluator;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMLogic;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.Player;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.moves.IdleAction;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.moves.PlaceBombAction;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.moves.TriggerBombAction;
import za.ac.sun.cs.ingenious.games.bomberman.network.UpdateBombsMessage;
import za.ac.sun.cs.ingenious.search.mcts.legacy.PlayoutPolicy;

/**
 * Random Bomberman playout policy for MCTS.
 */
public class BMRandomPolicy implements PlayoutPolicy<BMBoard> {
	
    public static final int DEFAULT_MAX_PLAYOUT_DEPTH = 80;
	private BMLogic logic = BMLogic.defaultBMLogic;
	private BMFinalEvaluator eval = new BMFinalEvaluator();
    private int maxPlayoutDepth;
	
	public BMRandomPolicy() {
        this(DEFAULT_MAX_PLAYOUT_DEPTH);
	}

    /**
     * @param maxPlayoutDepth Playouts will be scored with evaluation function after this many moves without reaching a terminal state.
     */
	public BMRandomPolicy(int maxPlayoutDepth) {
        this.maxPlayoutDepth = maxPlayoutDepth;
	}

    private boolean isSafeMove(BMBoard b, Action m, int curPlayer){
        if (m instanceof IdleAction || m instanceof PlaceBombAction || m instanceof TriggerBombAction){
            return !isBombNearby(b, curPlayer);
        }                  
        if (m instanceof CompassDirectionAction){
            CompassDirectionAction move = (CompassDirectionAction) m;
            Point newPos = BMBoard.getPos(b.getPlayers()[curPlayer].getPosition(), move.getDir());
            return !isBombNearby(b, newPos);
        }                  
        Log.error("Unknown move");
        return false;      
    }                      
                           
    /**                    
     * @param id Player id 
     *                     
     * @return Whether there is a bomb nearby the given player
     */                    
    private boolean isBombNearby(BMBoard b, int id){
        Player p = b.getPlayers()[id];
        return isBombNearby(b, p.getPosition());
    }                      
                           
    /**                    
     * @param position Co-ordinates on board
     *                     
     * @return Whether there is a bomb near the given co-ordinates
     */                    
    private boolean isBombNearby(BMBoard b, Point position){
        for (CompassDirection i: CompassDirection.cardinalDirections()){
            Point pos = position;
            while (b.isPassable(pos)) {
                pos = BMBoard.getPos(pos, i);
            }              
            if (b.isBomb(pos)) {
                return true;
            }              
        }                  
        return false;      
    }

    /**
     * Perform a single playout and return the result for each player.
     */
	@Override
	public za.ac.sun.cs.ingenious.search.mcts.legacy.PlayoutPolicy.PlayoutResult playout(BMBoard origBoard) {
		BMBoard board = (BMBoard) origBoard.deepCopy();
		int numPlayers = board.getPlayers().length;
		int currentPlayer = 0;
		if (board.isAlternating()) {
			currentPlayer = board.alternatingMovingPlayer;
		}
		int i = 0;
		while (!logic.isTerminal(board) && i < maxPlayoutDepth) {
			i++;
			if (board.getPlayer(currentPlayer).isAlive()) {
				List<Action> actions = logic.generateActions(board, currentPlayer);
				List<Action> unsafeMoves = new ArrayList<>();
				boolean safeMoveMade = false;
				while (!actions.isEmpty() && !safeMoveMade) {
					Action m = actions.remove((int) (Math.random() * actions.size()));
					if (isSafeMove(board, m, currentPlayer)) {
						logic.makeMove(board, m);
						Log.trace("Safe move: " + m.toString());
						safeMoveMade = true;
					} else {
						unsafeMoves.add(m);
					}
				}
				if (!safeMoveMade) {
					Action m = unsafeMoves.remove((int)(Math.random()*actions.size()));
					Log.trace("Unsafe move: " + m.toString());
					logic.makeMove(board, m);
				}
			}
			currentPlayer = (currentPlayer + 1) % numPlayers;
			if(currentPlayer==0){
				Log.trace("=========================");
				Log.trace("Round "+ (i / numPlayers));
				board.printPretty();
				logic.applyPlayedMoveMessage(new UpdateBombsMessage(-1, true), board); // Detonate bombs and generate sample upgrades if relevant
			}
		}
		Log.trace("Playout finished after round "+ (i / numPlayers));
		return new PlayoutResult(eval.getScore(board), i);
	}
}
