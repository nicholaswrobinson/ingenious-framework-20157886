package za.ac.sun.cs.ingenious.games.cardGames.hearts.game;

import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.games.cardGames.classicalDeck.ClassicalSuits;
import za.ac.sun.cs.ingenious.games.cardGames.classicalDeck.ClassicalSymbols;
import za.ac.sun.cs.ingenious.games.cardGames.core.rack.CardRack;

/**
 * The class UnoInitGameAction.
 * 
 * @author Joshua Wiebe
 */
public class HeartsInitGameMessage extends InitGameMessage{

	/** The serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The initial rack of a player. */
	private CardRack<ClassicalSymbols, ClassicalSuits> rack;
	
	/** The player ID for a specific player. */
	private int playerID;
	
	/** Number of players */
	private int numPlayers;
	/**
	 * Instantiates a new unoInitGameAction.
	 *
	 * @param rack Initial rack
	 * @param playerID player ID
	 * @param numPlayers number of players
	 */
	public HeartsInitGameMessage(CardRack<ClassicalSymbols, ClassicalSuits> rack, int playerID, int numPlayers){
		this.rack = rack;
		this.playerID = playerID;
		this.numPlayers = numPlayers;
	}
	
	/**
	 * Gets the rack.
	 *
	 * @return the rack
	 */
	public CardRack<ClassicalSymbols, ClassicalSuits> getRack(){
		return rack;
	}
	
	/**
	 * Gets player ID.
	 *
	 * @return player ID
	 */
	public int getPlayerID(){
		return playerID;
	}
	
	/**
	 * Gets number of players.
	 *
	 * @return number of players
	 */
	public int getNumPlayers(){
		return numPlayers;
	}
	
	
}
